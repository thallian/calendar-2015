# Calendar 2015
A calendar for the year 2015, built in LaTeX. The main work is done by the `calpage` command which is based on the [birthday calendar example](www.texample.net/tikz/examples/birthday-calendar/) by Hakon Malmedal.  

Of course the command is usable for years to come, not only 2015 ;)  

The images are licensed as [cc-by 4.0](ihttp://creativecommons.org/licenses/by/4.0/) as is calpage.sty.

# Prerequisites
- [LuaTeX](http://www.luatex.org/)
- some kind of shell if you want to use the build script (which does nothing fancy, it is mainly a memory hook)

# Usage
```
./build
```

Months and weekdays are in german, if you want to change that take a look at the `pgfcalendarmonthname` definition in `calendar.tex` and the `day letter headings` in `calpage.sty`. For holiday definitions, take a look at the end of `calpage.sty`.
I will make that easier to configure in a future version.

You'll find the generated pdf in a folder called `out`

![a screenshot of december](screenshot.png)
